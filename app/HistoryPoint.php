<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryPoint extends Model
{
    var $table = 'history_points';
}
