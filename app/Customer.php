<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    var $table = 'customers';
}
