<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware'=>'auth'],function(){

	Route::get('/home', 'HomeController@index')->name('home');
 	//Route::get('/', 	'HomeController@index')->name('home');
	
	//customer
	Route::get('/customer', 		'CustomerController@index')->name('customer');
	Route::get('/customer/{id}', 'CustomerController@show')->name('customer/{id}');

	Route::get('/customer/create', 	'CustomerController@create')->name('customer/create');
	

	Route::post('/customer', 'CustomerController@store');


	Route::put('customer/edit', 'CustomerController@edit')->name('customer/edit');
	//Route::get('customer/create', ['as' => 'customer.create', 'uses' => 'CustomerController@create']);
	// Route::get('customer/add', function(){
	// 		return view('customer.add');
	// 	});
	Route::get('/customer/add', 'CustomerController@add')->name('customer/add');

});