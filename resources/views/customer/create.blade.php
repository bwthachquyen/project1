@extends('layouts.app')
@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add customer form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('customer')}}">Customer</a></li>
              <li class="breadcrumb-item active">Add Customer</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
      <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">New customer</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <!-- /.card-header -->
          <form method="post" action="{{route('customer')}}"  enctype="multipart/form-data">
            @csrf
            <div class="card-body">
              <div class="row">
                 <div class="col-md-6">
                  <div class="form-group">
                <div class="m-b-10 text-center"><div class="radio-img-square"><img src="https://www.gravatar.com/avatar/d41d8cd98f00b204e9800998ecf8427e?s=80&amp;d=mp&amp;r=g" id="previewAvatar" alt="Avatar" width="120px"></div> <div class="form-group mt-2"><label for="inputAvatar" class="label-change">Thay đổi</label> <input type="file" id="inputAvatar" name="avatar" accept="image/png, image/jpeg, image/jpg" onchange="readURL(this);" class="form-control d-none"></div></div>
                </div>
              </div>

                
                <!-- /.col -->
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control my-colorpicker1 colorpicker-element" data-colorpicker-id="1" data-original-title="" title=""required autocomplete="name" autofocus value="{{old('name')}}" />
                    
                  </div>
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control my-colorpicker1 colorpicker-element" data-colorpicker-id="1" data-original-title="" title=""required autocomplete="email" autofocus/>
                  </div>

                  <div class="row">
                    <div class="col-md-6"><div class="form-group"><label for="inputBirthday">Ngày sinh</label> <div data-v-22cec9cc=""><input data-v-22cec9cc="" name="birthday" id="inputBirthday" type="text" autocomplete="off" placeholder="Y-m-d" class="form-control flatpickr-input" readonly="readonly"> <!----></div></div></div>
                   <div class="col-md-6">
                    <div class="form-group"><label for="inputSex">Giới tính</label>
                    <select name="sex" id="inputSex" class="form-control"><option value="male">Nam</option> <option value="female">Nữ</option></select></div>
                  </div>
                 </div>
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="numberic" name="phone" class="form-control my-colorpicker1 colorpicker-element" data-colorpicker-id="1" data-original-title="" title=""/>
                  </div>
                  <div class="form-group">
                    <label>Company</label>
                    <input type="text" class="form-control my-colorpicker1 colorpicker-element" data-colorpicker-id="1" data-original-title="" title=""/>
                  </div>
                  <div class="form-group">
                  <!-- <label>Date:</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate">
                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div> -->
                  <div class="form-group">
                    <label>Address</label>
                    <textarea type="text" name="name" class="form-control my-colorpicker1 colorpicker-element" data-colorpicker-id="1" data-original-title="" title=""></textarea>
                  </div>
                  <!--cmt-->

                  <div class="card-header">
                      <div class="card-tools"><button type="submit" class="btn btn-success float-right"> New customer
                  </button></div>
                    </div>
                  <!-- /.form-group -->
                </div>

                <!-- /.col -->
              </div>
              <!-- /.row -->
              
              <!-- /.row -->
            </div>


            
          </form>
          <!-- /.card-body -->
          <div class="card-footer" style="text-align: center;">
             <a href="#">Copyright © CNV Software LLC, all rights reserved. Powered by CNV Platform.</a> Version 1.5.8
          </div>
        </div>
    </section>

@endsection
