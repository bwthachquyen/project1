<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeService extends Model
{
    var $table = 'type_services';
}
